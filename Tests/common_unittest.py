# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import unittest

from Utils.StatusEnum import StatusEnum
from Utils.StringsNPS import get_string

from Common.CellDefinition import CellDefinition
from Common.SpeciesDefinition import SpeciesDefinition
from Common.InitialCondition import InitialCondition

from reaktoro import *
from dolfin import *


class unittest_commonMethods(unittest.TestCase):

    def test_cellDefinition(self):
        # Defines the known initial values
        pressure = 100000.0
        temperature = 298.15
        elementAmounts = [1.0, 111.016746658, 1.0, 55.5083733288, 0.0]
        volumeGas = 0.0
        volumeAq = 0.00101908026387
        volumeTotal = 0.00101908026387
        porosity = 1.0
        speciesAmountNa = 1.0

        # Creates a new cellDefinition()

        

        # Defines the chemical system
        gemsLocationFile = "./Tests/gems/nacl-dat.lst"
        chemSys = ChemicalSystem(Gems(gemsLocationFile))
        cell = CellDefinition(2, "OPA",Gems(gemsLocationFile).state(chemSys))

        # Verifies if the values are ok
        self.assertEqual(cell.name, "OPA")
        self.assertEqual(cell.index, 2)
        self.assertEqual(cell.pressure, pressure)
        self.assertEqual(cell.temperature, temperature)
        self.assertEqual(cell.porosity, porosity)
        for i in range(len(elementAmounts)):
            self.assertAlmostEqual(cell.element_amounts[i], elementAmounts[i])
        self.assertAlmostEqual(cell.volume_gas, volumeGas)
        self.assertAlmostEqual(cell.volume_aqueous, volumeAq)
        self.assertAlmostEqual(cell.volume_total, volumeTotal)
        self.assertAlmostEqual(cell.species_amount("Na+"), speciesAmountNa)

    def test_speciesDefinition(self):
        # defines the known initial values
        index = 0
        name = "Na+"
        charge = 1.0
        de = 1.0e-9

        # Defines the chemical system
        gemsLocationFile = "./Tests/gems/nacl-dat.lst"
        chemSys = ChemicalSystem(Gems(gemsLocationFile))

        species = SpeciesDefinition(index, chemSys.species()[0].name(), chemSys.species()[0].charge(), de)

        self.assertEqual(index, species.index)
        self.assertEqual(name, species.name)
        self.assertEqual(charge, species.charge)
        self.assertEqual(de, species.diffusionCoeff)

    def test_u_init(self):
        list_solution = []
        meshPath = "./Tests/1d.xml.gz"
        mesh = Mesh(meshPath)
        elementFamily = FiniteElement("CG", mesh.ufl_cell(), 1)
        ME_ONE_FE = FunctionSpace(mesh, elementFamily)
        U_LIST = Function(ME_ONE_FE)
        U_LIST.rename("Na+", 'label')

        # Defines the chemical system
        gemsLocationFile = "./Tests/gems/nacl-dat.lst"
        gemsLocationFileLow = "./Tests/gems/naclLOW-dat.lst"
        gemsObj = [Gems(gemsLocationFile), Gems(gemsLocationFileLow)]

        chemSyst = ChemicalSystem(Gems(gemsLocationFile))

        # Creating the list of cells
        chemLimits = [99, 199]
        listOfCells = []
        for i in range(200):

            # Defines which chemical system

            chemSysIndex = 0
            for j in range(len(chemLimits) - 1):
                if int(i) > int(chemLimits[j]):
                    chemSysIndex += 1

            # Creates a new cellDefinition()

            cell = CellDefinition(i, gemsObj[chemSysIndex], gemsObj[chemSysIndex].state(chemSyst))


            list_solution.append(cell.species_amount("Na+") / cell.porosity)

            # Appends the created cell to the list of cells

            listOfCells.append(cell)

        # Definitions of the u_init expression using u_init class
        u_init_expression = InitialCondition(
            name="Na+",
            list_of_cells=listOfCells,
            method="concentration",
            degree = 1)
        # Interpolation of the expression to the function space
        U_LIST.interpolate(u_init_expression)

        # Extracting the array with values
        calculate = project(U_LIST, ME_ONE_FE).vector().get_local()[::-1]

        # Comparison with the solution and the interpolated
        self.assertTrue(list_solution, calculate)


# Unit tests for the element utils class
print "common_unittest.py"
suite = unittest.TestLoader().loadTestsFromTestCase(unittest_commonMethods)
testResult = unittest.TextTestRunner(verbosity=0).run(suite)
if len(testResult.failures) > 0 or len(testResult.errors) > 0:
    print "Problem during tests in the COMMON classes."
    sys.exit(-1)
