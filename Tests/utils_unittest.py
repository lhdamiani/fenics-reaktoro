# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import unittest
from Utils.StatusEnum import StatusEnum
from Utils.StringsNPS import get_string

class unittest_utilsMethods(unittest.TestCase):

    # Tests of the statusEnum class
    def test_status(self):
        # Definition of error and success enums
        Error = 0
        Success = 1

        # Definition from the status enum class
        statusError = StatusEnum.get_error
        statusSuccess = StatusEnum.get_success

        # Test of the generated status enum class
        self.assertTrue(statusError, Error)
        self.assertTrue(statusSuccess, Success)

    # Tests of the string class
    def test_strings(self):
        stringVerify = "Testing strings class"
        self.assertTrue(get_string("STRING"), stringVerify)


# Unit tests for the common classes
print "utils_unittest.py"
suite = unittest.TestLoader().loadTestsFromTestCase(unittest_utilsMethods)
testResult = unittest.TextTestRunner(verbosity=0).run(suite)

if len(testResult.failures) > 0 or len(testResult.errors) > 0:
    print "Problem during tests in the UTILS classes."
    sys.exit(-1)
