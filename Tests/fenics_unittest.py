# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import sys
import unittest
from reaktoro import *
from dolfin import *


class unittest_fenicsMethods(unittest.TestCase):

    def test_fenics(self):
        error_L2_verification = 0.008235098073354943
        error_max_verification = 1.3322676295501878e-15
        # Create mesh and define function space
        mesh = UnitSquareMesh(8, 8)
        V = FunctionSpace(mesh, 'P', 2)

        # Define boundary condition
        u_D = Expression('1 + x[0]*x[0] + 2*x[1]*x[1]', degree=2)

        def boundary(x, on_boundary):
            return on_boundary

        bc = DirichletBC(V, u_D, boundary)

        # Define variational problem
        u = TrialFunction(V)
        v = TestFunction(V)
        f = Constant(-6.0)
        a = dot(grad(u), grad(v)) * dx
        L = f * v * dx

        # Compute solution
        u = Function(V)
        solve(a == L, u, bc)

        # Plot solution and mesh
        plot(u)
        plot(mesh)

        # Compute error in L2 norm
        error_L2 = errornorm(u_D, u, 'L2')

        # Compute maximum error at vertices
        vertex_values_u_D = u_D.compute_vertex_values(mesh)
        vertex_values_u = u.compute_vertex_values(mesh)
        import numpy as np
        error_max = np.max(np.abs(vertex_values_u_D - vertex_values_u))

        # Assert
        self.assertTrue(error_L2_verification, error_L2)
        self.assertTrue(error_max_verification, error_max)

print "fenics_unittest.py"
# Unit tests for the common classes
suite = unittest.TestLoader().loadTestsFromTestCase(unittest_fenicsMethods)
testResult = unittest.TextTestRunner(verbosity=0).run(suite)

if len(testResult.failures) > 0 or len(testResult.errors) > 0:
    print "Problem during tests in the FENICS test class."
    sys.exit(-1)
