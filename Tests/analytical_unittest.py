# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import sys
import os
import operator
import unittest

from Utils.StringsNPS import get_string
from Handlers.Core import make_core
from scipy.special._ufuncs import erfc
import numpy as np
from dolfin import *


class u_init_simplified(Expression):

    def __init__(self, **kwargs):
        self.valuesArray = kwargs["array"]

    def eval_cell(self, value, x, ufc_cell):
        value[0] = self.valuesArray[ufc_cell.index]


class unittest_analyticalMethods(unittest.TestCase):

    def test_analyticalSolution(self):
        arg = "./Demos/NaCl_diffusion/nacl_diffusion.nps"
        _parameters_list = []
        # opens the
        inputfile = open("./" + arg)

        _id = False
        for line in inputfile:
            if not _id:
                if get_string("ID") not in line:
                    print get_string("INPUT")
                    sys.exit()
                else:
                    _id = True
            if (get_string("COMMENT_KEYWORD") not in line or
                    "#" not in line) and len(line) > 5:
                _parameters_list.append(line.split())
        if len(_parameters_list) > 0.0:
            name = get_string("SCRIPT_NAME")
            _script_name = [name, "=", arg.split("/")[-1]]
            param1 = arg.split("/")[1] + "/"
            param2 = arg.split("/")[2] + "/"
            _script_param = get_string("SCRIPT_PATH")
            _script_path = [_script_param, "=", "./" + param1 + param2]
            _parameters_list.append(_script_name)
            _parameters_list.append(_script_path)
            # Verification for DC and BC files
            is_file_dc = os.path.isfile(
                "./" + arg + get_string("DC_EXT"))
            is_file_bc = os.path.isfile(
                "./" + arg + get_string("BC_EXT"))
            is_file_wf = os.path.isfile(
                "./" + arg + get_string("WF_EXT"))
            if (is_file_dc and is_file_bc and is_file_wf) is False:
                print get_string("FILE_MISSING")
                sys.exit()

            # Diffusion Coefficients
            _id = False
            _external_dc_file = open(
                "./" + arg + get_string("DC_EXT"), 'r')
            _external_dc_values = []
            for line in _external_dc_file:
                if not _id:
                    if get_string("ID") not in line:
                        print get_string("INPUT")
                        sys.exit()
                    else:
                        _id = True
                com_keyword = get_string("COMMENT_KEYWORD")
                end_keyword = get_string("END_KEYWORD")
                if (com_keyword not in line) \
                        and len(line) > 5 \
                        and "#" not in line \
                        and (end_keyword not in line):
                    de_name = line.split(" = ")[0]
                    de_value = line.split(" = ")[1][:-1]
                    _external_dc_values.append([de_name, de_value])

            _external_dc_file.close()
            inputfile.close()

            sys.setrecursionlimit(17000)
            nps_core = make_core(_parameters_list, _external_dc_values)
            nps_core.run_simulation()

            totalTime = 1.0e2
            xVector = []
            concRight = 0.5
            concLeft = 1.0
            positionLeft = -0.01
            positionRight = 0.01
            tolPos = 1.0e-10
            tolNeg = -1.0e-10
            analytics = []
            spaceStep = 0.02 / 200
            while positionLeft < positionRight + tolPos:
                if tolNeg < positionLeft < tolPos:
                    xVector.append(0.0)
                else:
                    xVector.append(round(positionLeft, 4))
                positionLeft += spaceStep
            # calculates the solution for all cells and generates a list of the
            # solutions
            adjust = spaceStep / 2

            analytics = []
            for i in range(len(xVector)):
                analytics.append((concRight - concLeft) * 0.5 *
                                 erfc(-(xVector[i] + adjust) / (np.sqrt(4 * 1.6106286393345E-9 * totalTime))) + concLeft)

            xdmfFileOutputAnalytical = XDMFFile(nps_core.get_mesh().mpi_comm(),
                                                "./Tests/analytical.xdmf")
            functionExample = Function(nps_core.get_function_space())
            initValue = u_init_simplified(array=analytics,
                                          element=nps_core.get_function_space().ufl_element())
            functionExample.interpolate(initValue)
            xdmfFileOutputAnalytical.write(functionExample, totalTime)
            analytics = analytics[::-1]
            vectorSolution = nps_core.get_solution_vector(nps_core.get_u_list_0(), nps_core.get_function_space(), 0)
            diff = [analytics[i] - vectorSolution[i] for i in range(len(xVector))]
            index, value = max(enumerate(diff), key=operator.itemgetter(1))

            # Assert 
            self.assertTrue(index, 101)
            self.assertTrue(value, 0.0173101716977)


print "analytical_unittest.py"
# Unit tests for the common classes
suite = unittest.TestLoader().loadTestsFromTestCase(unittest_analyticalMethods)
testResult = unittest.TextTestRunner(verbosity=0).run(suite)

if len(testResult.failures) > 0 or len(testResult.errors) > 0:
    print "Problem during tests in the analytical solution."
    sys.exit(-1)
