# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



class MineralDefinition:
    """
    Definition of the mineral containing index, name and index of species present in the mineral.
    """

    def __init__(self, index, name, index_of_species):
        self._index = name
        self._name = name
        self._index_of_species = index_of_species

    # pressure [Pascal]
    @property
    def index(self):
        """
        Property to get the index of the mineral.
        :return: Index of the mineral.
        """
        return self._index

    # temperature [K]
    @property
    def mineral(self):
        """
        Property to get the name of the mineral.
        :return: Name of the mineral.
        """
        return self._name

    @property
    def index_of_species(self):
        """
        Method to get the list with indexes of species in mineral.
        :return: List with indexes of species.
        """
        return self._index_of_species
