# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Definition of a species


class SpeciesDefinition:
    """
    Species definition including index, name, charge and diffusion coefficient.
    """

    def __init__(self, index, name, charge, dc):
        self._index = index
        self._name = name
        self._charge = charge
        self._diffusion_coeff = dc

    @property
    def name(self):
        """
        Method to get the name of the species.
        :return: Name of the species.
        """
        return self._name

    @property
    def charge(self):
        """
        Method to get the charge of the species.
        :return: Charge of the species.
        """
        return self._charge

    @property
    def diffusionCoeff(self):
        """
        Method to get the diffusion coefficient of the species.
        :return: Diffusion coefficient of the species.
        """
        return self._diffusion_coeff

    @property
    def index(self):
        """
        Method to get the index of the species.
        :return: Index of the species.
        """
        return self._index
