# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
from dolfin import *

class PorosityExpression(Expression):
    def __init__(self, list_of_cells, list_diff_coeff, degree):
        self._list_of_cells = list_of_cells
        self._list_diff_coeff = list_diff_coeff
        self._last_index = -1

    def eval_cell(self, value, x, ufc_cell):
        if len(self._list_diff_coeff) == 0:
            cell = self._list_of_cells[ufc_cell.index]
            porosity = cell.porosity
            if porosity > 1.0:
                value[0] = 1.0
            else:
                value[0] = porosity
            # print value[0]
        else:
            cell = self._list_of_cells[ufc_cell.index]
            diff_coeff_index = 0
            for j in range(len(self._list_diff_coeff) - 1):
                if int(cell.index) >= int(self._list_diff_coeff[j][1]):
                    diff_coeff_index += 1
            if self._last_index != cell.index:
                # print cell.index, self._list_diff_coeff[diff_coeff_index]
                self._last_index = cell.index
                value[0] = float(self._list_diff_coeff[diff_coeff_index][0])

