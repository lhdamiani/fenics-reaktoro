# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from scipy import stats

class FactorExpression(Expression):
    def __init__(self, xleft, xright, factor, left_factor, right_factor, degree):
        self._factor = factor
        self._xleft = xleft
        self._xright = xright
        self._left_factor = left_factor
        self._right_factor = right_factor

    def eval(self, value, x):
        tol = 1e-14
        value[0] = self._factor
        if x[0] <= self._xleft + tol:
            value[0] = self._left_factor
        if x[0] >= self._xright - tol:
            value[0] = self._right_factor

class DeExpression(Expression):
    def __init__(self, mesh, degree, deConc, deCox,indexNode):
        self._last_index = -1
        self._mesh = mesh
        self._deConcrete = deConc
        self._deCox = deCox
        self._indexNode = indexNode
    def eval_cell(self, value, x, ufc_cell):
        diff_coeff_index = 0
        index = ufc_cell.index
        if self._last_index != index:
            self._last_index = index
            if index < self._indexNode:
                value[0] = self._deConcrete
            elif index > self._indexNode + 1:
                value[0] = self._deCox
            else:
                value[0] = stats.hmean([self._deCox, self._deConcrete])

class concExpression(Expression):
    def __init__(self, mesh, degree, concConc, concCox, indexNode):
        self._last_index = -1
        self._mesh = mesh
        self._concCox = concCox
        self._concConc = concConc
        self._indexNode = indexNode
    def eval_cell(self, value, x, ufc_cell):
        diff_coeff_index = 0
        index = ufc_cell.index
        if self._last_index != index:
            self._last_index = index
            if index < self._indexNode:
                value[0] = self._concConc
            elif index > self._indexNode+ 1:
                value[0] = self._concCox
            else:
                value[0] = (self._concConc) / 2