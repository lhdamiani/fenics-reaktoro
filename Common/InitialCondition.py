# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
from dolfin import *


class InitialCondition(Expression):
    # Initialize the parameters
    def __init__(self, method, name, list_of_cells, degree):
        self._method = method
        self._name = name
        self._list_of_cells = list_of_cells

    def eval_cell(self, value, x, ufc_cell):
        concentration_species = 0.0
        cell = self._list_of_cells[ufc_cell.index]
        if self._method == "concentration":  # mol/m^3
            if cell.volume_aqueous > 0.0:
                concentration_species += cell.species_aq_concentration(self._name)
        elif self._method == "amount":  # mol/m^3
            concentration_species += cell.species_amount(self._name)
        elif self._method == "amount_solid": 
            concentration_species += cell.phase_amount(self._name)
        elif self._method == "amount_solid_m3": 
            concentration_species += cell.phase_amount(self._name)*1000
        elif self._method == "concentration_molkgw":  # mol/kgw
            concentration_species += cell.species_aq_concentration_molkgw(self._name)
        else:
            print "Problem with method in InitialCondition class"
            sys.exit()
        value[0] = concentration_species
