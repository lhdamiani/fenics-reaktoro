# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



class CellDefinition(object):
    """
    Definition of the chemical cell containing index, name and chemical state.
    """

    def __init__(self,i,name, chemical_state):
        self._index = i
        self._name = name
        self._chemical_state = chemical_state


    @property
    def chemical_state(self):
        """
        Prtessure of the system.
        :return:
        """
        return self._chemical_state

    # pressure [Pascal]
    @property
    def pressure(self):
        """
        Prtessure of the system.
        :return:
        """
        return self._chemical_state.pressure()

    # temperature [K]
    @property
    def temperature(self):
        """
        Temperature of the system.
        :return:
        """
        return self._chemical_state.temperature()

    # Elemental composition
    @property
    def element_amounts(self):
        """
        Method to fetch the elemental composition of a chemical state.
        :return: Total element amounts in a vector.
        """
        return self._chemical_state.elementAmounts()

    # Elemental composition
    @property
    def element_amount(self, index):
        """
        Method to fetch the elemental composition of a chemical state.
        :return: Total element amounts in a vector.
        """
        return self._chemical_state.elementAmount(index)

    # volume [m^3]
    @property
    def volume_gas(self):
        """
        Method to get the volume of the gas phase.
        :return: Volume of the gas phase
        """
        if len(self._chemical_state.properties().phaseVolumes().val) > 1:
            return float(self._chemical_state.properties().phaseVolumes().val[1])
        else:
            return 0.0

    @property
    def volume_aqueous(self):
        """
        Method to get the aqueous phase volume.
        :return: Volume of the aqueous phase volume.
        """
        # print self._chemical_state.properties().phaseVolumes().val
        # try:
        return float(self._chemical_state.properties().phaseVolumes().val[0])
        # except:
        #     return 1.0e-20

    @property
    def index(self):
        """
        Method to get the index of the cell
        :return: Index of the cell
        """
        return self._index

    @property
    def name(self):
        """
        Method to get the name of the cell
        :return: Name of the cell
        """
        return self._name

    @property
    def volume_total(self):
        """
        Method to get the total volume of the chemical state.
        :return: Total volume of the chemical state.
        """
        acc = 0.0
        for val in self._chemical_state.properties().phaseVolumes().val:
            acc += val
        return acc

    @property
    def porosity(self):
        """
        Method to get the porosity of the chemical state.
        :return: Porosity of the chemical state.
        """
        return self.volume_aqueous / self.volume_total

    # returns the amount of the species [mol]
    def species_amount(self, name):
        """
        Method to get the amount of specific species in the chemical state.
        :param name: Name of the species.
        :return: Amount of the specified species in the chemical system.
        """
        return self._chemical_state.speciesAmount(name)

    def species_aq_concentration(self, name):
        """
        Method to get the concentration (mol/m3)of specific species in the aqueous phase chemical state.
        :param name: Name of the species.
        :return: Aqueous concentration of the specified species in the chemical system.
        """
        return self._chemical_state.speciesAmount(name) / self.volume_aqueous

    def species_aq_concentration_molkgw(self, name):
        """
        Method to get the concentration (mol/kgw) of specific species in the aqueous phase chemical state.
        :param name: Name of the species.
        :return: Aqueous concentration of the specified species in the chemical system.
        """
        return self._chemical_state.speciesAmount(name) / (self._chemical_state.speciesAmount("H2O@") / 55.508)

    # returns the amount of the species [mol]
    def set_species_amount(self, name, value):
        """
        Method to set the amount of specific species in the chemical state.
        :param name: Name of the species.
        :param value: Mol amount of the species.
        """
        self._chemical_state.setSpeciesAmount(name, value)

    # returns the amount of the species [mol]
    def phase_amount(self, name):
        """
        Method to get the amount of specific phase in the chemical state.
        :param index: Name of the phase.
        """
        return self._chemical_state.phaseAmount(name)
    
    def set_porosity(self, phase, value):
        """ Method to set the phase amount according the
         porosity value defined by the user.
        
        Arguments:
            phase {string} -- name of the phase that will be scaled to adjust the porosity
            value {float} -- Value of the desired porosity.
        """
        if value > 0.0:
            newVolume = (1-float(value))*self.volume_aqueous / float(value)
            self._chemical_state.scalePhaseVolume(phase,newVolume)