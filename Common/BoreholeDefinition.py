# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



class BoreholeDefinition:
    """
    Definition of the borehole definition containing the properties.
    """

    def __init__(self, index, name, bulk, chemical_state, area_bh):
        self._index = index
        self._name = name
        self._bulk = bulk
        self._chemical_state = chemical_state
        self._area_bh = area_bh

    @property
    def index(self):
        """
        Property to get index of the borehole.
        :return: Index of the borehole.
        """
        return self._index

    @property
    def name(self):
        """
        Property to get name of the borehole.
        :return: Name of the borehole
        """
        return self._name

    @property
    def bulk(self):
        """
        Property to get the bulk composition of the borehole.
        :return: Bulk elemental composition in the borehole.
        """
        return self._bulk

    @property
    def chemical_state(self):
        """
        Property to get the chemical state of the borehole.
        :return: Chemical state of the borehole.
        """
        return self._chemical_state

    @property
    def area_bh(self):
        """
        Property to get the surface area of the borehole.
        :return: Surface area of the borehole
        """
        return self._area_bh

    @property
    def pressure(self):
        """
        Property to get the pressure of the borehole.
        :return: Pressure in the borehole cell.
        """
        return self._chemical_state.pressure()

    @property
    def pressure_in_atm(self):
        """
        Property to get the pressure of the borehole.
        :return: Pressure in the borehole cell.
        """
        return self._chemical_state.pressure() *9.86923e-6   

    @property
    def temperature(self):
        """
        Property to get the pressure of the borehole.
        :return: Pressure in the borehole cell.
        """
        return self._chemical_state.temperature()

    @property
    def volume_aq(self):
        """
        Property to get the aqueous volume inside the borehole.
        :return: Aqueous volume in the borehole
        """
        return self._chemical_state.properties().phaseVolumes().val[0]

    @property
    def volume_gas(self):
        """
        Property to get the gaseous volume inside the borehole.
        :return: Gaseous volume in the borehole
        """
        return self._chemical_state.properties().phaseVolumes().val[1]

    @property
    def volume_total(self):
        """
        Property to get the total volume inside the borehole.
        :return: Total volume in the borehole
        """
        acc = 0.0
        for val in  self._chemical_state.properties().phaseVolumes().val:
            acc += val
        return acc
        
    @property
    def chemical_state(self):
        """
        Property to get the total amount in the gaseous phase of the borehole.
        :return: Amount in gaseous phase.
        """
        return self._chemical_state.phaseAmount("Gaseous")

    def speciesAmount(self, name):
        """
        Property to get a specific species amount in the borehole.
        :return: Amount in gaseous phase.
        """
        return self._chemical_state.speciesAmount(name)
