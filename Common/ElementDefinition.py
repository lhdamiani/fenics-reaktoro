# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



class ElementDefinition:
    """
    Definition of the element containing index and name.
    """

    def __init__(self, index, name):
        self._index = index
        self._name = name

    @property
    def index(self):
        """
        Property to get the index of an element.
        :return: Index of the element
        """
        return self._index

    @property
    def name(self):
        """
        Property to get the name of an element.
        :return: Name of the element.
        """
        return self._name
