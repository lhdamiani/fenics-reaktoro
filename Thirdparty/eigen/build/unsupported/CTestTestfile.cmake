# CMake generated Testfile for 
# Source directory: /home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported
# Build directory: /home/lhdamiani/Documents/nps/Thirdparty/eigen/build/unsupported
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(Eigen)
subdirs(doc)
subdirs(test)
