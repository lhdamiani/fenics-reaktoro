# Install script for directory: /home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/unsupported/Eigen" TYPE FILE FILES
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/AdolcForward"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/AlignedVector3"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/ArpackSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/AutoDiff"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/BVH"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/EulerAngles"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/FFT"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/IterativeSolvers"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/KroneckerProduct"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/LevenbergMarquardt"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/MatrixFunctions"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/MoreVectorization"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/MPRealSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/NonLinearOptimization"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/NumericalDiff"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/OpenGLSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/Polynomials"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/Skyline"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/SparseExtra"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/SpecialFunctions"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/Splines"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/unsupported/Eigen" TYPE DIRECTORY FILES "/home/lhdamiani/Documents/nps/Thirdparty/eigen/unsupported/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/lhdamiani/Documents/nps/Thirdparty/eigen/build/unsupported/Eigen/CXX11/cmake_install.cmake")

endif()

