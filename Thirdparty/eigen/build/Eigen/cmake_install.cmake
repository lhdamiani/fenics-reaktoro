# Install script for directory: /home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE FILE FILES
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Sparse"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Householder"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/SPQRSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/SparseCholesky"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Geometry"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/SuperLUSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/SVD"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Cholesky"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/SparseCore"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/StdDeque"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/StdVector"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Eigen"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Eigenvalues"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/SparseLU"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/OrderingMethods"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/SparseQR"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Core"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/LU"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/KLUSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Jacobi"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/StdList"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/PardisoSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/UmfPackSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/QR"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/Dense"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/PaStiXSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/CholmodSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/MetisSupport"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/IterativeLinearSolvers"
    "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/QtAlignedMalloc"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE DIRECTORY FILES "/home/lhdamiani/Documents/nps/Thirdparty/eigen/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

