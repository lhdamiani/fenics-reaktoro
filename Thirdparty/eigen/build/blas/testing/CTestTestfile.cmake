# CMake generated Testfile for 
# Source directory: /home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing
# Build directory: /home/lhdamiani/Documents/nps/Thirdparty/eigen/build/blas/testing
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(sblat1 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "sblat1" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/sblat1.dat")
add_test(sblat2 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "sblat2" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/sblat2.dat")
add_test(sblat3 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "sblat3" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/sblat3.dat")
add_test(dblat1 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "dblat1" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/dblat1.dat")
add_test(dblat2 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "dblat2" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/dblat2.dat")
add_test(dblat3 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "dblat3" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/dblat3.dat")
add_test(cblat1 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "cblat1" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/cblat1.dat")
add_test(cblat2 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "cblat2" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/cblat2.dat")
add_test(cblat3 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "cblat3" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/cblat3.dat")
add_test(zblat1 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "zblat1" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/zblat1.dat")
add_test(zblat2 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "zblat2" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/zblat2.dat")
add_test(zblat3 "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/runblastest.sh" "zblat3" "/home/lhdamiani/Documents/nps/Thirdparty/eigen/blas/testing/zblat3.dat")
