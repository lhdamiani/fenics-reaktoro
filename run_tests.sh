#!/usr/bin/env bash
#
# This script runs the tests of NPS and benchmarks.
# Author: Leonardo Hax Damiani <leonardo.hax@psi.ch>

echo 'tests'
python -m unittest discover ./ "*_unittest.py"

echo 'Diffusion'
python nps.py -i ./Demos/NaCl_diffusion/nacl_diffusion.nps

echo 'Lichter'
python nps.py -i ./Demos/Lichtner_Benchmark/lichtner.nps

echo 'Membrane 8'
python nps.py -i ./Demos/MembraneDiffusion8/membraneDiffusion8.nps

echo 'Membrane 19'
python nps.py -i ./Demos/MembraneDiffusion19/membraneDiffusion19.nps

echo 'Charged Membrane'
python nps.py -i ./Demos/ChargedMembrane/charged.nps

echo 'Advective Calcite Dolomite fronts'
python nps.py -i ./Demos/Bench5/bench5.nps

echo 'Constriction'
python nps.py -i ./Demos/NaCl_constriction/nacl_constriction.nps

#echo 'Cebama'
#python nps.py -i ./Demos/Cebama_Benchmark/cebama.nps