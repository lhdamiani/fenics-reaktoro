# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


def getF(eqMode,
         U_LIST,
         V_LIST,
         U_LIST_0,
         listOfSpecies,
         F,
         dielectric,
         R,
         T,
         dt,
         du,
         por,
         mesh,
         dsExternal,
         me_fe_one,
         dpEff, porEff):

    duList = split(du)
    adv_v = Constant(9.375e-6)
    # adv_v_sqrt = Constant(8.7890625e-11)
    disp_v = Constant(9.375e-6 * 0.0067)
    h = CellSize(mesh)

    F = V_LIST[0]*duList[0]*dx - V_LIST[0]*U_LIST_0[0]*dx+ dt*adv_v*duList[0]*V_LIST[0].dx(0)*dx
    F += Constant(0.32) * dt * (listOfSpecies[0].diffusionCoeff + disp_v) * inner(nabla_grad(duList[0]),nabla_grad(V_LIST[0]))*dx
    for i in range(len(listOfSpecies)):
        if i != 0:
            diffusionEff = (listOfSpecies[i].diffusionCoeff + disp_v)
            adv = dt*adv_v*duList[i]*V_LIST[i].dx(0)*dx
            dif = Constant(0.32) * dt * diffusionEff * inner(nabla_grad(duList[i]),nabla_grad(V_LIST[i]))*dx
            F += V_LIST[i]*duList[i]*dx - V_LIST[i]*U_LIST_0[i]*dx - adv + dif

    return rhs(F), lhs(F)


def getProblem(F, u, bcs, du):
    return LinearVariationalProblem(F[1], F[0], u, bcs)


def getSolver(problem):
    return LinearVariationalSolver(problem)

def getSolution(L, a, u, bcs):
    A = assemble(a)
    for i in bcs:
        i.apply(A)
    solver = LUSolver(A)
    solver.parameters["reuse_factorization"] = True

    b = assemble(L)
    for i in bcs:
        i.apply(b)

    # Solve the linear system (re-use the already factorized matrix A)
    solver.solve(u.vector(), b)
    return 1