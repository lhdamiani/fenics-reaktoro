import copy
import sys, os

from reaktoro import *

import time as tm
import numpy as np

########################
# CHEMICAL COMPOSITION #
########################
# Use an exported project file from GEMS to initialize a Gems object
gemsLeft = Gems("cox-dat.lst")
gemsRight = Gems("concrete-dat.lst")

# and then use it to construct the ChemicalSystem object.
reaktoroSystemLeft = ChemicalSystem(gemsLeft)
reaktoroSystemRight = ChemicalSystem(gemsRight)

# Create a ChemicalState object that contains the temperature, pressure,
# and amounts of species stored in the exported GEMS file.
chemStateLeft = gemsLeft.state(reaktoroSystemLeft)
chemStateRight = gemsRight.state(reaktoroSystemRight)

# defining the options for the equilibrium solver
reaktoroOptions = EquilibriumOptions()
reaktoroOptions.optimum.output.active = False
reaktoroOptions.optimum.tolerance = 1.0e-6
# reaktoroOptions.optimum.regularization.echelonize = False

bulkLeft = chemStateLeft.elementAmounts()
bulkRight = chemStateRight.elementAmounts()



equilibriumsolver = EquilibriumSolver(reaktoroSystemLeft)
equilibriumsolver.setOptions(reaktoroOptions)

listB = []
reaktoroStates = []


for i in range(10):
    if i < 10 / 2:
        reaktoroStates.append(gemsLeft.state(reaktoroSystemLeft))
        reaktoroStates[-1].setSpeciesAmounts(0.0)
        listB.append(bulkLeft)
    else:
        reaktoroStates.append(gemsRight.state(reaktoroSystemRight))
        reaktoroStates[-1].setSpeciesAmounts(0.0)
        listB.append(bulkRight)

# Equilibration method in all reaktoro states (nodes)
for (state,b) in zip(reaktoroStates,listB):
    equilibriumsolver.solve(state, state.temperature(), state.pressure(), b)

print 'finished successfully'
