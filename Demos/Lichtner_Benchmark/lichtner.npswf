# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from Common.FactorExpression import FactorExpression

def getF(eqMode,
         U_LIST,
         V_LIST,
         U_LIST_0,
         listOfSpecies,
         F,
         dielectric,
         R,
         T,
         dt,
         du,
         por,
         mesh,
         dsExternal,
         me_fe_one,
         dpEff, porEff):
    duList = split(du)
    a =  dot(duList[0],V_LIST[0])*dx
    for i in range(len(listOfSpecies)):
        if i != 0:
            a += dot(duList[i], V_LIST[i])*dx
    for i in range(len(listOfSpecies)):
        a +=  dt * inner( listOfSpecies[i].diffusionCoeff * nabla_grad(duList[i]), nabla_grad(V_LIST[i])) * dx
    A = assemble(a)

    L =  dot(U_LIST_0[0], V_LIST[0])*dx

    for i in range(len(listOfSpecies)):
        if i != 0:
            L += dot(U_LIST_0[i], V_LIST[i])*dx

    summationTerm = sum([listOfSpecies[i].charge**2 * listOfSpecies[i].diffusionCoeff * U_LIST_0[i]
        for i in range(len(listOfSpecies))])

    for i in range(len(listOfSpecies)):
        for k in range(len(listOfSpecies)):
            L += dt * inner(listOfSpecies[i].charge * listOfSpecies[k].charge * \
                                        listOfSpecies[i].diffusionCoeff * listOfSpecies[k].diffusionCoeff * \
                                        U_LIST_0[i] / ( summationTerm ) * \
                                        nabla_grad(U_LIST_0[k]), nabla_grad(V_LIST[i]))*dx

    return L,a


def getProblem(F, u, bcs, du):
    return LinearVariationalProblem(F[1], F[0], u, bcs)


def getSolver(problem):
    solver = LinearVariationalSolver(problem)
    return solver

def getSolution(solver, F, u, bcs):
    solver.solve()
    return 1