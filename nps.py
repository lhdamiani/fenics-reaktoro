#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# NPS is an open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""
    nps.py
    ~~~~~~~~~~~~~~ 

    NPS is an open-source code for reactive transport simulations.
"""

import sys
import os
import getopt
import os.path

# NPS files
from Utils.StringsNPS import get_string
from Handlers.Core import make_core


# main function to load input file and run simulation
def main(argv):
    """ Reads in the input file's parameters and verifies if the necessary files exists (DC, BC and WF).

    :param argv: Initial arguments for nps reactive transport solver.
    """
    try:
        opts, arg = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print get_string("HELP")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            get_string("HELP")
            sys.exit()
        elif opt in ("-i", "--ifile"):
            _parameters_list = []
            # opens the
            inputfile = open("./" + arg)

            # ID verification
            _id = False
            for line in inputfile:
                if not _id:
                    if get_string("ID") not in line:
                        print get_string("INPUT")
                        sys.exit()
                    else:
                        _id = True
                if (get_string("COMMENT_KEYWORD") not in line or
                        "#" not in line) and len(line) > 5:
                    _parameters_list.append(line.split())
            if len(_parameters_list) > 0.0:
                name = get_string("SCRIPT_NAME")
                _script_name = [name, "=", arg.split("/")[-1]]
                param1 = arg.split("/")[1] + "/"
                param2 = arg.split("/")[2] + "/"
                _script_param = get_string("SCRIPT_PATH")
                _script_path = [_script_param, "=", "./" + param1 + param2]
                _parameters_list.append(_script_name)
                _parameters_list.append(_script_path)
                # Verification for DC and BC files
                is_file_dc = os.path.isfile(
                    "./" + arg + get_string("DC_EXT"))
                is_file_bc = os.path.isfile(
                    "./" + arg + get_string("BC_EXT"))
                is_file_wf = os.path.isfile(
                    "./" + arg + get_string("WF_EXT"))
                if (is_file_dc and is_file_bc and is_file_wf) is False:
                    print get_string("FILE_MISSING")
                    sys.exit()

                # Diffusion Coefficients
                _id = False
                _external_dc_file = open(
                    "./" + arg + get_string("DC_EXT"), 'r')
                _external_dc_values = []
                for line in _external_dc_file:
                    if not _id:
                        if get_string("ID") not in line:
                            print get_string("INPUT")
                            sys.exit()
                        else:
                            _id = True
                    com_keyword = get_string("COMMENT_KEYWORD")
                    end_keyword = get_string("END_KEYWORD")
                    if (com_keyword not in line) \
                            and len(line) > 5 \
                            and "#" not in line \
                            and (end_keyword not in line):
                        de_name = line.split(" = ")[0]
                        de_value = line.split(" = ")[1][:-1]
                        _external_dc_values.append([de_name, de_value])

                _external_dc_file.close()
                inputfile.close()

                # print sys.getrecursionlimit()
                # quit()
                sys.setrecursionlimit(17000)
                nps_core = make_core(_parameters_list, _external_dc_values)
                nps_core.run_simulation()
            else:
                print get_string("PARAMETERS")
                sys.exit(-1)


if __name__ == "__main__":
    # Simulating arguments to open the input file
    # sys.argv.append("-i")
    # sys.argv.append(get_string("1D_NACL_DIFFUSION"))
    # sys.argv.append(get_string("2D_NACL_CONSTRICTION_CHARGED"))
    # sys.argv.append(get_string("LICHTNER_BENCH"))
    # sys.argv.append(get_string("MEMBRANE_DIFFUSION_8"))
    # sys.argv.append(get_string("MEMBRANE_DIFFUSION_19"))
    # sys.argv.append(get_string("CEM_CLAY_INTERFACE"))
    # sys.argv.append(get_string("CEBAMA_BENCH"))
    main(sys.argv[1:])
