#!/usr/bin/python
# -*- coding: utf-8 -*-
# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import glob
from reaktoro import *
from dolfin import *


import time as tm
from datetime import datetime

from Common.CellDefinition import CellDefinition
from Common.SpeciesDefinition import SpeciesDefinition
from Common.InitialCondition import InitialCondition
from Common.PorosityExpression import PorosityExpression
from Common.FactorExpression import DeExpression
from Common.FactorExpression import concExpression

from Utils.Logging import Logging
from Utils.StatusEnum import StatusEnum
from Utils.StringsNPS import get_string

if has_linear_algebra_backend("Epetra"):
    parameters["linear_algebra_backend"] = "Epetra"


# Generates an object of the Core class

def make_core(*args):
    return Core(*args)

# Core class


class Core(object):
    # Core initialization routine

    def __init__(self, *args):
        if len(args) > 0:
            init_file_path = "./Handlers/CoreInit.py"
            local_dict_init = locals()
            exec (compile(open(init_file_path).read(),
                          init_file_path, 'exec'), globals(), local_dict_init)

    # Get Finite Element function space
    def get_function_space(self):
        """Method to get the function space of the simulation.
        Returns:
            Function Space -- The function space chosen for the simulation.
        """

        return self._ME_ONE_FE

    # Get mesh
    def get_mesh(self):
        """Method to get the mesh of the simulation.

        Returns:
            Mesh -- The mesh chosen for the simulation.
        """
        return self._mesh


    # Get previous solution
    def get_u_list_0(self):
        """Method to get the mesh of the simulation.

        Returns:
            Mesh -- The mesh chosen for the simulation.
        """

        return self._U_LIST_0

    # Get projection of the solution
    def get_solution_vector(self, list_me, me_fe_one, index):
        """Method to project solution into a vector based on the index of the solution.

        Arguments:
            index {Integer} -- The index of the solution wanted.

        Returns:
            Vector array -- The vector projection of the solution with index.
        """
        # Projects the solution into a variable
        sol_proj = project(list_me[index], me_fe_one)
        # Returns the array verion of the projections
        return sol_proj.vector().get_local()[::-1]

    # NPS core run simulation method
    def run_simulation(self):
        """Run simulation Core method.
        """

        # Saves initial time clock
        cpu_time_start = tm.clock()
        # Saves initial date time
        start_simulation_time = str(datetime.now())
        # Prints initialization label
        self._log.status_print(get_string('INIT_NPS'),
                               True)

        ########
        # TIME #
        ########
        if self._status == StatusEnum.get_success:
            self._log.status_print(get_string('INIT') + get_string('TIME'), False)
            # initial time
            t = 0
            # time step
            dt = Constant(self._timeStep)
            # total time
            total_time = Constant(self._totalTime)
            self._status = StatusEnum.get_success
        else:
            self._log.status_print(' Problem with the temporal definition...',
                                   False)
            quit()

        ########
        # MESH #
        ########
        self._log.status_print(get_string('INIT') + get_string('MESH'), False)
        if self._status == StatusEnum.get_success:
            # Creates a FEniCS mesh object with the mesh from the file
            mesh = Mesh(self._scriptPath + self._meshFile)
            self._mesh = mesh
            num_cells = mesh.num_cells()
            self._status = StatusEnum.get_success
        else:
            self._log.status_print(
                ' Problem with the mesh definition...', False)
            quit()

        ###################
        # CHEMICAL SYSTEM #
        ###################
        if self._status == StatusEnum.get_success:
            self._log.status_print(get_string('INIT') + get_string('CHEM'), False)
            # Creates the Reaktoro chemical system based on the files
            gems_obj = []
            reak_systems = []
            for i in range(len(self._fileChemList)):
                gems_location_file = str(self._scriptPath + self._fileChemList[i])
                gems_obj.append(Gems(gems_location_file))
                reak_systems.append(ChemicalSystem(gems_obj[-1]))

            # Defining the equilibrium_options
            reaktoro_options = EquilibriumOptions()
            reaktoro_options.optimum.output.active = False
            reaktoro_options.optimum.tolerance = 1.0e-7

            # Defining the equilibrium_solver
            equilibrium_solver = EquilibriumSolver(reak_systems[0])
            equilibrium_solver.setOptions(reaktoro_options)

            # Creating the list of cells
            list_of_cells = []
            for i in range(num_cells):
                # Defines which chemical system
                chem_sys_index = 0
                for j in range(len(self._chemLimits) - 1):
                    if int(i) > int(self._chemLimits[j]):
                        chem_sys_index += 1
                # Creates a new cellDefinition()
                cell = CellDefinition(i,self._fileChemList[chem_sys_index],gems_obj[chem_sys_index].state(reak_systems[chem_sys_index]))

                # Scales up to 1 L
                cell.chemical_state.scaleVolume(0.001)

                bulk = cell.chemical_state.elementAmounts()
                if self._transportOnly != "yes":
                    cell.chemical_state.setSpeciesAmounts(0.0)
                    equilibrium_solver.solve(cell.chemical_state,
                                            cell.temperature, cell.pressure,
                                            bulk)
                list_of_cells.append(cell)

                # Defining the system temperature
                if self._temperature == '':
                    self._temperature = Constant(cell.temperature)

            # Including water to the list of ignored species transported
            self._ignoreTransportSpecies.append('H2O@')
            # Including solid phases to not transported species
            for phase in reak_systems[0].phases():
                if phase.name() != get_string('AQ_PHASE'):
                    for species in phase.species():
                        self._ignoreTransportSpecies.append(species.name())

            # Creating list of species
            list_of_species = []
            species_counter = 0
            for species in reak_systems[0].species():
                # Solid phases and water are not transported
                if species.name() not in self._ignoreTransportSpecies:
                    diff_coeff = self._listOfDCs[-1][1]
                    for j in self._listOfDCs:
                        if species.name() == j[0]:
                            diff_coeff = float(j[1])
                    if len(self._diffCoeffList) == 0:
                        speciesNew = SpeciesDefinition(species_counter,
                                                species.name(),
                                                Constant(species.charge()),
                                                Constant(diff_coeff))
                    else:
                        speciesNew = SpeciesDefinition(species_counter,
                                                species.name(),
                                                Constant(species.charge()),
                                                Constant(1.0))
                    species_counter += 1
                    list_of_species.append(speciesNew)
            self._status = StatusEnum.get_success
        else:
            self._log.status_print(
                get_string('PROB_CHEM'), False)
            sys.exit(1)

        #############################
        # FINITE ELEMENT DEFINITION #
        #############################
        if self._status == StatusEnum.get_success:
            self._log.status_print(get_string('INIT') + get_string('FE'), False)
            element_family = FiniteElement(str(self._feFamily),
                                          mesh.ufl_cell(), int(self._feDegree))

            # For each species, 1 finite element unknown
            list_fe = []
            for i in list_of_species:
                list_fe.append(element_family)
            # If poisson equation is solved +1
            if self._transportEq == 'PNP':
                list_fe.append(element_family)
            # Mixed element space
            mixed_function_space = MixedElement(list_fe)

            # Creates the mixed finite element function space
            me_fe = FunctionSpace(mesh, mixed_function_space)

            # Creates the finite element function space
            me_fe_one = FunctionSpace(mesh, element_family)
            self._ME_ONE_FE = me_fe_one

            # Trial and test functions
            u = TrialFunction(me_fe)
            v = TestFunction(me_fe)
            u_list = split(u)
            v_list = split(v)

            # Compiler parameters
            parameters['form_compiler']['cpp_optimize'] = True
            parameters['form_compiler']['optimize'] = True
            parameters['ghost_mode'] = 'shared_facet'
            h = 2 * Circumradius(mesh)
            n = FacetNormal(mesh)
            alpha = Constant(1.0)
            self._status = StatusEnum.get_success
        else:
            self._log.status_print(get_string('PROB_FE'),
                                   False)
            sys.exit(1)

        #####################
        # INITIAL CONDITION #
        #####################
        if self._status == StatusEnum.get_success:
            u_list_0 = []
            self._log.status_print(get_string('INIT') +
                                   get_string('IC'),
                                   False)
            for i in list_of_species:
                u_list_0.append(Function(me_fe_one))
                u_list_0[-1].rename(i.name, 'label')
                # Initial condition based on chemical nodes and aqueous concentration of the species
                u_init_expression = InitialCondition(
                    name=i.name,
                    list_of_cells=list_of_cells,
                    method=get_string('INIT_MODE_CONC'),
                    degree = 1)
                u_list_0[-1].interpolate(u_init_expression)

            if self._transportEq == "PNP":
                u_list_0.append(Function(me_fe_one))
                u_list_0[-1].rename(str(get_string('PSI_LABEL')),
                                    'label')
                exp_null = Expression('00.0', element=me_fe_one.ufl_element())
                u_list_0[-1].interpolate(exp_null)
            self._status = StatusEnum.get_success
        else:
            self._log.status_print(get_string('PROB_IC'),
                                   False)
            sys.exit(1)

        #######################
        # BOUNDARY CONDITIONS #
        #######################
        if self._status == StatusEnum.get_success:
            self._log.status_print(get_string('INIT') + get_string('BC_LABEL'), False)
            bcs = []
    
            # Defines the path to the NPS boundary condition file
            bc_file_path = self._scriptPath + self._scriptName + get_string('BC_EXT')
            local_dict_bc = locals()
            exec (compile(open(bc_file_path).read(), bc_file_path, 'exec'), globals(), local_dict_bc)
            bcs = local_dict_bc['bcs']

            # Internal facets
            dsExternal = None
            if self._scriptName == "nacl_constriction.nps":
                facets2 = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
                facets2.set_all(0)
                TopElectrode().mark(facets2, 10)
                BottomElectrode().mark(facets2, 20)
                ElectrodeLeft().mark(facets2, 30)
                ElectrodeRight().mark(facets2, 40)
                Left().mark(facets2, 50)
                Right().mark(facets2, 60)
                dsExternal = Measure("ds")(subdomain_data=facets2)

            self._status = StatusEnum.get_success
        else:
            self._log.status_print(get_string('PROB_BC'), False)
            sys.exit(1)

        ##################################
        # WEAK FORM - PROBLEM AND SOLVER #
        ##################################
        if self._status == StatusEnum.get_success:
            self._log.status_print(get_string('INIT') + get_string('WF_LABEL'), False)
            u = Function(me_fe, name=get_string('SPECIES_LABEL' ))
            du = TrialFunction(me_fe)
            v_list = TestFunctions(me_fe)
            wf_file_path = self._scriptPath + self._scriptName + get_string('WF_EXT')
            exec (compile(open(wf_file_path).read(), wf_file_path, 'exec'), globals())
            self._status = StatusEnum.get_success
        else:
            self._log.status_print(get_string('PROB_WF'), False)
            sys.exit(1)

        #############
        # LOG LEVEL #
        #############
        if self._status == StatusEnum.get_success:
            # Create progress bar
            progress = Progress('Time-stepping')
            set_log_level(PROGRESS)
            set_log_active(False)
            self._status = StatusEnum.get_success
        else:
            self._log.status_print(' Problem with the log level...', False)
            quit()

        ##################################
        # Von Neumann diffusion criteria #
        ##################################
        mesh_dimension = mesh.topology().dim()
        fastestDe = -1.0
        smallestXStep = -1.0
        if mesh_dimension == 1:
            for species in list_of_species:
                if float(species.diffusionCoeff) > fastestDe:
                    fastestDe = float(species.diffusionCoeff)
        mesh_coordinates = mesh.coordinates()
        for index in range(len(mesh_coordinates)):
            if index > 0:
                distance_X = mesh_coordinates[index][0] - mesh_coordinates[index-1][0]
                if smallestXStep < distance_X:
                    smallestXStep = distance_X
        highestTimeStep = pow(distance_X,2) / (3 * fastestDe)

        ##########
        # OUTPUT #
        ##########
        if self._status == StatusEnum.get_success:
            self._log.status_print(get_string('INIT') + get_string('OUTPUT'), False)

            # Creates the output directory if not exists
            if not os.path.exists(self._scriptPath + 'output/'):
                os.makedirs(self._scriptPath + 'output/')
            else:
                # Cleans the directory for the new files
                for f in glob.glob(self._scriptPath + 'output/*'):
                    os.remove(f)
            output_files_list = []
            for i in range(len(me_fe.split())):
                if i < len(list_of_species) and self._outputTxtSpace == "no":
                    previous_solution = u_list_0[i]
                    species_name = list_of_species[i].name
                    output_file_name = self._scriptPath + \
                                     get_string('OUTPUT_DIR') + \
                                     self._scriptName[:-4] + \
                                     get_string('OUTPUT_NAME') + \
                                     species_name + \
                                     get_string('OUTPUT_EXTENSION')

                    xdmf_file_output = XDMFFile(mesh.mpi_comm(), output_file_name)
                    output_files_list.append(xdmf_file_output)
                    output_files_list[-1].write(previous_solution, float(t))
            # If equation is Poisson-Nernst-Planck
            if self._transportEq == "PNP":
                # PSI file name
                psi_file_name = self._scriptPath + get_string('OUTPUT_DIR') \
                              + self._scriptName[:-4] + get_string('OUTPUT_PSI')

                # PSI xdmf file
                psi_xdmf_file = XDMFFile(mesh.mpi_comm(), psi_file_name)
                # Adds it to the list of output files
                output_files_list.append(psi_xdmf_file)
                # Saves previous solution IC
                previous_solution = u_list_0[len(list_of_species)]
                output_files_list[-1].write(previous_solution, float(t))
            # Creates the output file for solids
            solid_xdmf_files = []
            if self._transportOnly != "yes"  and self._outputTxtSpace == "no":
                # Creates the list of minerals to be printed out
                if len(self._listOfSolidFunction) == 0:
                    # for each extra phase (excluding the aqueous pahse 1)
                    for i in range(1, len(reak_systems[0].phases())):
                        # Creates a function space for each mineral
                        self._listOfSolidFunction.append(Function(me_fe_one))
                        # Renames to the mineral's name
                        self._listOfSolidFunction[-1].rename(reak_systems[0].phase(i).name(),'label')
                        # Appends to the list that will be used to print
                        self._listOfSolidVectors.append([])
                        # Solids file name
                        solid_file_name = self._scriptPath \
                                    + get_string('OUTPUT_DIR') \
                                    + self._scriptName[:-4] \
                                    + reak_systems[0].phase(i).name() \
                                    + get_string('OUTPUT_SOLIDS')
                        # Solids xdmf file
                        solid_xdmf_files.append(XDMFFile(mesh.mpi_comm(), solid_file_name))

                for i in range(1, len(reak_systems[0].phases())):
                    solidExpression = InitialCondition(name=reak_systems[0].phases()[i].name(),
                                                    list_of_cells=list_of_cells,
                                                    method= get_string('INIT_MODE_AMOUNT_SOLID'),
                                                    degree = 1)
                    self._listOfSolidFunction[i - 1].interpolate(solidExpression)
                    solid_xdmf_files[i-1].write(self._listOfSolidFunction[i - 1], float(t))

            ###################
            # Txt output file #
            ###################
            if self._scriptName == "membraneDiffusion19.nps" or self._scriptName == "membraneDiffusion8.nps" :
                outputfileLeft =  self._scriptPath + \
                                        get_string('OUTPUT_DIR') + \
                                        self._scriptName[:-4] + ".txt"
                text_fileExp19 = open(outputfileLeft, "w")
                stringSpecies = ""
                for i in list_of_species:
                    stringSpecies += i.name+"_left,"+i.name+"_right,"
                text_fileExp19.write("time, "+stringSpecies+"\n")

            # Cebama benchmark
            if self._outputTxtSpace != "no":

                T_10Y = 3.154e+6    # 10 years
                T_100Y = 3.154e+9   # 100 years
                T_1000Y = 3.154e+10 # 1k years
                T_5000Y = 1.5768e+11 # 5k years
                T_10000Y = 3.1536e+11 # 10k years

                output0, output10, output100, output1000, output5000, output10000 = 0, 0, 0, 0, 0, 0
                list_of_outputTimes = [(3.154e+8,10),
                                       (3.154e+9,100),
                                       (3.154e+10,1000),
                                       (1.5768e+11,5000),
                                       (3.1536e+11,10000)]
                counter_output_file = 0
                list_of_outputFiles = []
                file_name_0y = self._scriptPath + \
                                        get_string('OUTPUT_DIR') + \
                                        self._scriptName[:-4] + \
                                        get_string('OUTPUT_NAME') + "_0y.txt"
                text_file_0y = open(file_name_0y, "w")
                list_of_outputFiles.append(text_file_0y)
                file_name_10y = self._scriptPath + \
                                        get_string('OUTPUT_DIR') + \
                                        self._scriptName[:-4] + \
                                        get_string('OUTPUT_NAME') + "_10y.txt"
                text_file_10y = open(file_name_10y, "w")
                list_of_outputFiles.append(text_file_10y)
                file_name_100y = self._scriptPath + \
                                        get_string('OUTPUT_DIR') + \
                                        self._scriptName[:-4] + \
                                        get_string('OUTPUT_NAME') + "_100y.txt"
                text_file_100y = open(file_name_100y, "w")
                list_of_outputFiles.append(text_file_100y)
                file_name_1000y = self._scriptPath + \
                                        get_string('OUTPUT_DIR') + \
                                        self._scriptName[:-4] + \
                                        get_string('OUTPUT_NAME') + "_1000y.txt"
                text_file_1000y = open(file_name_1000y, "w")
                list_of_outputFiles.append(text_file_1000y)
                file_name_5000y = self._scriptPath + \
                                        get_string('OUTPUT_DIR') + \
                                        self._scriptName[:-4] + \
                                        get_string('OUTPUT_NAME') + "_5000y.txt"
                text_file_5000y = open(file_name_5000y, "w")
                list_of_outputFiles.append(text_file_5000y)
                file_name_10000y = self._scriptPath + \
                                        get_string('OUTPUT_DIR') + \
                                        self._scriptName[:-4] + \
                                        get_string('OUTPUT_NAME') + "_10000y.txt"
                text_file_10000y = open(file_name_10000y, "w")
                list_of_outputFiles.append(text_file_10000y)
                output_file_header = "x, porosity, pH, pe, ionic_strength, totalVolume,"
                # Phase header and components
                for i in range(reak_systems[0].numPhases()):
                    phase_name = reak_systems[0].phases()[i].name()
                    output_file_header += phase_name + "_amount, "+phase_name+"_volume,"
                    for species in reak_systems[0].phases()[i].species():
                        output_file_header += phase_name+"_"+species.name()+","
                # Species header
                for i in range(len(me_fe.split())):
                    if i < len(list_of_species):
                        species_name = list_of_species[i].name
                        output_file_header += species_name + "_IMPORTANT,"
                # Elements header
                for i in range(reak_systems[0].numElements()):
                    elements_name = reak_systems[0].elements()[i].name()
                    if i < len(cell.element_amounts) - 1:
                        output_file_header += elements_name + ","
                    else:
                        output_file_header += elements_name + "\n"

                for i in range(len(list_of_outputTimes)):
                    list_of_outputFiles[i].write(output_file_header)

                for index, x in enumerate(mesh.coordinates()):
                    # porosity, pH, pe, ionic_strength
                    if index < len(list_of_cells):
                        porosity = float(list_of_cells[index].porosity)
                        pH = list_of_cells[index].chemical_state.properties().aqueous().pH().val
                        pE = list_of_cells[index].chemical_state.properties().aqueous().pe().val
                        ionic_strength = float(list_of_cells[index].chemical_state.properties().aqueous().ionicStrength().val)
                        totalVolume = float(list_of_cells[index].volume_total)
                    else:
                        porosity = 0.0
                        pH = 0.0
                        pE = 0.0
                        ionic_strength = 0.0
                        totalVolume = 0.0

                    results_str = str(x[0])+","+str(porosity)+","+str(pH)+","+str(pE)+","+str(ionic_strength)+","+str(totalVolume)+","
                    for i in range(reak_systems[0].numPhases()):
                        if index < len(list_of_cells):
                            phase_amount = list_of_cells[index].chemical_state.phaseAmount(i)
                            phase_vol = list_of_cells[index].chemical_state.properties().phaseVolumes().val[i]
                            results_str += str(phase_amount) + "," +str(phase_vol)+ ","
                            for species in reak_systems[0].phases()[i].species():
                                species_amount = list_of_cells[index].species_amount(species.name())
                                results_str += str(species_amount) + ","
                    for i in range(len(me_fe.split())):
                        results_str += str(u_list_0[i](x[0]))+","
                    for i in range(reak_systems[0].numElements()):
                        if index < len(list_of_cells):
                            element_amount = list_of_cells[index].chemical_state.elementAmountInPhase(i,0)
                            if i < reak_systems[0].numElements() - 1:
                                results_str += str(element_amount) + ","
                            else:
                                results_str += str(element_amount) + "\n"
                    list_of_outputFiles[counter_output_file].write(results_str)
                list_of_outputFiles[counter_output_file].close()
                counter_output_file += 1
            indexNode = 15
            dpEff = DeExpression(mesh = mesh, degree = 1, deConc = 2.5e-11 , deCox = 1.44e-10, indexNode=indexNode)
            porEff = DeExpression(mesh = mesh, degree = 1, deConc = 0.04, deCox = 0.18, indexNode=indexNode)
            self._status = StatusEnum.get_success
        else:
            self._log.status_print(get_string('PROB_OUTPUT'), False)
            sys.exit(1)

        # Prints out initialization finished successfully
        self._log.status_print(get_string('INIT_OK'),True)
        # Calculates the porosity including the diffusion coefficients
        por = PorosityExpression(list_of_cells=list_of_cells,
                    list_diff_coeff=self._diffCoeffList, degree=1)

        ###########################
        # REACTIVE TRANSPORT LOOP #
        ###########################
        if self._status == StatusEnum.get_success:
            while float(t) <= float(total_time):
                ####################
                # Variational Form #
                ####################
                # Weak formulation
                F = getF(self._transportEq, split(u), v_list, u_list_0,
                         list_of_species, self._faradayConstant,
                         self._dielectricConstant, self._gasConstant,
                         self._temperature, Constant(float(dt)), du, por, 
                         self._mesh, dsExternal, me_fe_one, dpEff, porEff)
                # Variational problem
                problem = getProblem(F, u, bcs, du)
                # Variational solver
                solver = getSolver(problem)

                ####################
                # Transport solver #
                ####################
                getSolution(solver, F, u, bcs)

                # Copies updated solution to previous solution array
                u_list = u.split(True)
                for i in range(len(u_list)):
                    u_list_0[i].assign(u_list[i])

                #########################
                # TRANSPORT -> CHEMICAL #
                #########################
                # Verifies if the simulation requires only transport solver
                if self._transportOnly != "yes":
                    # solutionBuffer is an array indexed by species concentration in mol/m^3
                    solution_buffer = []
                    for species in list_of_species:
                        # gets the projection of the species solution
                        projection = self.get_solution_vector(u_list_0, me_fe_one, species.index)
                        # appends to the solution buffer
                        solution_buffer.append(projection)
                    # calculates the mol amounts of each cell based on the concentrations and the aqueous phase
                    nodes_species_concentrations_in_mol = []
                    bulk_original = []
                    for cell in list_of_cells:
                        bulk_original.append(cell.element_amounts)
                        conc_buffer = []
                        for species in list_of_species:
                            # converts each element concentrations to element mol amounts
                            # bulkBuffer contains amounts [mol] => solution buffer [mol/m^3] * phasevolume [m^3]
                            conc_buffer.append(float(solution_buffer[species.index][cell.index] * cell.volume_aqueous))
                        # appends the list of mol amounts of each species in each cell
                        nodes_species_concentrations_in_mol.append(conc_buffer)
                    # Putting the values from the concentration array to each cell
                    for cell in list_of_cells:
                        # if cell.index != 98:
                        for species in list_of_species:
                            # if mol amount higher than zero for specific cell and species index
                            if nodes_species_concentrations_in_mol[cell.index][species.index] > 0.0:
                                # Saves the new amount of the species into the cell
                                cell.set_species_amount(species.name, nodes_species_concentrations_in_mol[cell.index][species.index])

                    ##################
                    # Chemical solver #
                    ##################
                    for cell in list_of_cells:
                        # Chemical equilibration based on the chemical state, temperature, pressure and bulk composition
                        bulk = cell.element_amounts
                        # speciesAmount = []
                        # for species in reak_systems[0].phases()[0].species():
                        #     speciesAmount.append(list_of_cells[index].species_amount(species.name()))
                        # cell.chemical_state.setSpeciesAmounts(0.0)
                        try: 
                            equilibrium_solver.solve(cell.chemical_state, cell.temperature,
                                            cell.pressure, bulk)
                        except:
                            print 'ERROR EQUILIBRATION in cell ', cell.index
                            equilibrium_solver.solve(cell.chemical_state, cell.temperature,
                                            cell.pressure, bulk_original[cell.index])
                            pass
                        # Saves the updated phase amounts of minerals 
                        for i in range(1, len(reak_systems[0].phases())):
                            if len(self._listOfSolidVectors) >= 2:
                                phase_name = reak_systems[0].phases()[i].name()
                                self._listOfSolidVectors[i - 1].append(cell.phase_amount(phase_name))


                    #########################
                    # CHEMICAL -> TRANSPORT #
                    #########################
                    for species in list_of_species:
                        # Converst the amounts of each aqueous species in each cell to concentrations for transport solver
                        u_list_0[species.index].interpolate(InitialCondition(name=species.name, list_of_cells=list_of_cells,
                            method=get_string('INIT_MODE_CONC'), degree = 1))


                # incrementing time
                t = float(t) + float(dt)
                if self._timeStepIncrease != 1.0:
                    dt = float(dt*float(self._timeStepIncrease))


                #################
                # SAVING OUTPUT #
                #################
                # Output aqueous species
                for i in range(len(me_fe.split())):
                    if i < len(list_of_species) and self._outputTxtSpace == "no":
                        previous_solution = u_list_0[i]
                        output_files_list[i].write(previous_solution, float(t))
                if self._transportEq == "PNP":
		            previous_solution = u_list_0[len(list_of_species)]
		            output_files_list[-1].write(previous_solution, float(t))

                # Output solid minerals
                if self._transportOnly != "yes":
                    for i in range(1, len(reak_systems[0].phases())):
                        if len(self._listOfSolidVectors) >= 2:
                            solidExpression = InitialCondition(name=reak_systems[0].phases()[i].name(),
                                                        list_of_cells=list_of_cells,
                                                        method="amount_solid_m3",
                                                        degree = 1)
                            self._listOfSolidFunction[i - 1].interpolate(solidExpression)
                            solid_xdmf_files[i-1].write(self._listOfSolidFunction[i - 1], float(t))

                if self._outputTxtSpace != "no":
                    # Defines the path to the NPS boundary condition file
                    outputTxtSpacePath = self._scriptPath + self._scriptName + "output"
                    local_dict_bc = locals()
                    exec (compile(open(outputTxtSpacePath).read(), outputTxtSpacePath, 'exec'), globals(), local_dict_bc)
                    output10 = local_dict_bc['output10']
                    output100 = local_dict_bc['output100']
                    output1000 = local_dict_bc['output1000']
                    output5000 = local_dict_bc['output5000']
                    output10000 = local_dict_bc['output10000']

                if self._scriptName == "bench5.nps":
                    file_name_left = "output_bench5.txt"
                    text_file_left = open(file_name_left, "w")
                    text_file_left.write("x, ca+2, mg+2, cl-, calcite, dolomite-ord\n") 
                    listOfProjections = [project(u_list_0[1],me_fe_one).vector().get_local()[::-1],
                                        project(u_list_0[3],me_fe_one).vector().get_local()[::-1],
                                        project(u_list_0[6],me_fe_one).vector().get_local()[::-1],
                                        project(self._listOfSolidFunction[0],me_fe_one).vector().get_local()[::-1],
                                        project(self._listOfSolidFunction[1],me_fe_one).vector().get_local()[::-1]]
                    for index, x in enumerate(mesh.coordinates()):
                        stringResult = str(x[0])+","+str(listOfProjections[0][index])+","+\
                        str(listOfProjections[1][index])+","+str(listOfProjections[2][index])+","+\
                        str(listOfProjections[3][index])+","+str(listOfProjections[4][index])+"\n"
                        text_file_left.write(stringResult)
                    text_file_left.close()
                if self._scriptName == "membraneDiffusion19.nps" or self._scriptName == "membraneDiffusion8.nps" :
                    stringCurrentTime = str(t)+","
                    for i in range(len(list_of_species)):
                        species_solutionProject = project(u_list_0[i],me_fe_one).vector().get_local()
                        stringCurrentTime += str(species_solutionProject[-2]/1000)+","+ str(species_solutionProject[1]/1000)+","
                    text_fileExp19.write(stringCurrentTime+"\n")
                # Updates the previous solution reference
                self._U_LIST_0 = u_list_0
                # Update progress bar
                if self._iteration % 2 == 0:
                    # Ratio of current t and total time
                    self._log.update_progress(float(t) / float(total_time))
                # Incrementing iteration counter
                self._iteration += 1

            if self._scriptName == "charged.nps":
                file_name_left = self._scriptPath + \
                                get_string('OUTPUT_DIR') + \
                                self._scriptName[:-4] + ".txt"
                text_file_left = open(file_name_left, "w")
                text_file_left.write("x, so4-2, mg+2, na+, k+, psi\n") 
                listOfProjections = [project(u_list[3],me_fe_one).vector().get_local()[::-1], 
                                    project(u_list[1],me_fe_one).vector().get_local()[::-1], 
                                    project(u_list[2],me_fe_one).vector().get_local()[::-1], 
                                    project(u_list[0],me_fe_one).vector().get_local()[::-1], 
                                    project(u_list[4],me_fe_one).vector().get_local()[::-1]]
                for index, x in enumerate(mesh.coordinates()):
                    stringResult = str(x[0])+","+str(listOfProjections[0][index])+","+\
                    str(listOfProjections[1][index])+","+str(listOfProjections[2][index])+","+\
                    str(listOfProjections[3][index])+","+str(listOfProjections[4][index])+"\n"
                    text_file_left.write(stringResult)
                text_file_left.close()


            # Calculates total time of reactive transport loop
            cpu_time_total = tm.clock() - cpu_time_start
            # Converts cpu time to hours or minutes
            if cpu_time_total / 3600 < 1.0:
                cpu_time_total = cpu_time_total / 60
                output_time = '%2.2f' % cpu_time_total + get_string('MIN')
            else:
                cpu_time_total = cpu_time_total / 3600
                output_time = '%0.4f' % cpu_time_total + get_string('HRS')
            # End of simulation date
            end_simulation_time = str(datetime.now())

            # Prints out output directory
            self._log.print_finished(output_time, self._scriptPath +
                                     get_string('OUTPUT_DIR'))
            # Prints out start/end date and simulation time
            self._log.status_print(
                get_string('START_DATE') +
                start_simulation_time +
                get_string('END_DATE') +
                end_simulation_time +
                '\n',
                False)

