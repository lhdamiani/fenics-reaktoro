# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# parameters list
self._totalTime = 0.0
self._timeStep = 0.0
self._feFamily = ''
self._feDegree = 0
self._fileChemList = []
self._scriptName = ''
self._scriptPath = ''
self._transportOnly = ''
self._transportEq = ''
self._chemLimits = []
self._dielectricConstant = ''
self._faradayConstant = ''
self._gasConstant = ''
self._temperature = ''
self._voltage = Constant(0.0)
self._U_LIST_0 = []
self._mesh = 0
self._diffCoeffList = []
self._ME_ONE_FE = None
self._listOfSolidFunction = []
self._listOfSolidVectors = []
self._porosity = 0.0
self._outputTxt = "no"
self._outputTxtSpace = "no"
self._timeStepIncrease = 1.0
self._ignoreTransportSpecies = []

for i in range(len(args[0])):

    # Total time
    if 'totalTime' in args[0][i][0]:
        self._totalTime = Constant(float(args[0][i][2]))

    # Time step
    if 'timeStep' in args[0][i][0]:
        self._timeStep = Constant(float(args[0][i][2]))

    # Mesh file path
    if 'meshFile' in args[0][i][0]:
        self._meshFile = args[0][i][2]

    # Finite element family
    if 'feFamily' in args[0][i][0]:
        self._feFamily = args[0][i][2]

    # Finite element degree
    if 'feDegree' in args[0][i][0]:
        self._feDegree = args[0][i][2]

    # List of chemical files
    if 'fileChem' in args[0][i][0]:
        self._fileChemList.append(args[0][i][2])
        self._chemLimits.append(args[0][i][3])

    # Script name
    if 'scriptName' in args[0][i][0]:
        self._scriptName = args[0][i][2]
    if 'scriptPath' in args[0][i][0]:
        self._scriptPath = args[0][i][2]

    # Finite element family
    if 'transportEq' in args[0][i][0]:
        self._transportEq = args[0][i][2]

    # Finite element family
    if 'transportOnly' in args[0][i][0]:
        self._transportOnly = args[0][i][2]

    # Dielectric constant
    if 'dielectricConstant' in args[0][i][0]:
        self._dielectricConstant = Constant(args[0][i][2])

    # Dielectric constant
    if 'faradayConstant' in args[0][i][0]:
        self._faradayConstant = Constant(args[0][i][2])

    # Gas constant
    if 'gasConstant' in args[0][i][0]:
        self._gasConstant = Constant(args[0][i][2])

    # Voltage
    if 'voltage' in args[0][i][0]:
        self._voltage = Constant(args[0][i][2])

    # User defined porosity
    if 'porosityMembrane' in args[0][i][0]:
        self._porosity = Constant(args[0][i][2])

    # Alternative txt output time
    if 'outputTxt' in args[0][i][0]:
        self._outputTxt = args[0][i][2]

    # Alternative txt output space
    if 'outputSpaceTxt' in args[0][i][0]:
        self._outputTxtSpace = args[0][i][2]

    # Material diffusion coefficients 
    if 'diffCoeffList' in args[0][i][0]:
        self._diffCoeffList.append((args[0][i][2], args[0][i][3]))
    
    # Time step increase
    if 'increaseNeumann' in args[0][i][0]:
        self._timeStepIncrease = args[0][i][2]

    # Ignore species in the transport
    if 'ignoreSpeciesTransport' in args[0][i][0]:
        for j in range(len(args[0][i]) - 2):
            self._ignoreTransportSpecies.append(args[0][i][j+2])

# Logging object
self._log = Logging(self._scriptName)

# List of species diffusion coefficients from dc file
self._listOfDCs = args[1]

# Raw parameters
self._parameters = args[0]

# Status enum
self._status = StatusEnum.get_success

# Iteration counter
self._iteration = 0