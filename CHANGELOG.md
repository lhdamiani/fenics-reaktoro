# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [Unreleased]

## [0.1.0] - 2018-05-02
### Added
- New modular version of NPS.
- Benchmark nacl diffusion.
- Benchmark membrane diffusion experiments #8 & #19.
- Benchmark lichtner.
- NPS docker machine.

### Changed
- Unit tests.

## [0.2.0] - 2018-07-22
### Added
- Benchkmark advection inflow MgCl2 calcite-dolomite precipitation dissolution fronts.
- Benchmark charged membrane diffusion.

### Changed
- Solver moved to weak formulation file.
- Ignored transpor species from input file.


## [0.2.1] - 2018-07-24
### Added
- Benchmark constricted charged pore space.