#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from Utils.Bcolors import Bcolors


# Class responsible for all the constant strings in the software.


def get_string(name):
    """
    Returns the string from the dict.
    """
    if name in StringsNPS.strings_const:
        return StringsNPS.strings_const[name]


class StringsNPS(type):
        """Dict class containing: Debugging messages / Loading messages / Statuses messages
        """
        strings_const = {
                "HELP": "Usage:\n" +
                        "1) \"nernstplanck_solver.py -i inputfile.nps\" : " +
                        "Simulation details are defined with the information" +
                        " from the input file.\n" +
                        "2) \"nernstplanck_solver.py -h\" : Details about the " +
                        "format of the input file.\n",
                "LOADING_TIMER": "Loading time handler...",
                "TIME": "time...",
                "MESH": "mesh...",
                "CHEM": "chemical composition...",
                "FE": "finite element framework...",
                "IC": "initial conditions...",
                "WF_LABEL": "weak formulation...",
                "OUTPUT": "preparing output files...",
                "BC_LABEL": "boundary conditions...",
                "INPUT": "Problem with the input file, " +
                        "please use \"nernstplanck_solver.py -h\" " +
                        " to verify the correct usage.",
                "PARAMETERS": "Problem with the list of parameters. " +
                        "Please check input file.",
                "FILE_MISSING": "Problem with the BC, DC or WF...",
                "NO_SOLUTE": "How to run a transport diffusion " +
                        "solver without solutes?",
                "UPDATE_FLOAT_ERROR": "Error : progress var must be float\r\n",
                "ANALYTICAL": "Analytical",
                "TITLE": Bcolors.OKBLUE + "\rNPS" + Bcolors.ENDC,
                "END_KEYWORD": "end",
                "COMMENT_KEYWORD": "##",
                "ID": "NPS",
                "SCRIPT_NAME": "scriptName",
                "SCRIPT_PATH": "scriptPath",
                "WF_EXT": "wf",
                "BC_EXT": "bc",
                "DC_EXT": "dc",
                "PROB_CHEM": "Problem with the chemical composition definition...",
                "PROB_FE": "Problem with the finite element definition...",
                "PROB_BC": "Problem with the boundary conditions...",
                "PROB_OUTPUT": "Problem with the output files...",
                "PROB_WF": "Problem with the weak formulation...",
                "PROB_IC": "Problem with the initial conditions...",
                "T": "totalTime",
                "1D_NACL_DIFFUSION": "./Demos/NaCl_diffusion/nacl_diffusion.nps",
                "LICHTNER_BENCH": "./Demos/Lichtner_Benchmark/lichtner.nps",
                "CEM_CLAY_INTERFACE" : "./Demos/Cement-Clay_interface/cem-clay.nps",
                "CEBAMA_BENCH": "./Demos/Cebama_Benchmark/cebama.nps",
                "MEMBRANE_DIFFUSION_8": "./Demos/MembraneDiffusion8/membraneDiffusion8.nps",
                "MEMBRANE_DIFFUSION_19": "./Demos/MembraneDiffusion19/membraneDiffusion19.nps",
                "2D_NACL_CONSTRICTION_CHARGED": "./Demos/NaCl_constriction/nacl_constriction.nps",
                "SPECIES": "Testing species class",
                "U_INIT": "Testing initialization class",
                "CELL": "Testing cell class",
                "OUTPUT_DIR": "output/",
                "OUTPUT_PSI": "_output_PSI.xdmf",
                "OUTPUT_SOLIDS": "_output_solids.xdmf",
                "OUTPUT_NAME": "_output_",
                "OUTPUT_EXTENSION": ".xdmf",
                "AQ_PHASE": "aq_gen",
                "SPECIES_LABEL": "SpeciesConcentrations",
                "INIT_MODE_CONC": "concentration",
                "INIT_MODE_AMOUNT": "amount",
                "INIT_MODE_AMOUNT_SOLID": "amount_solid",
                "PSI_LABEL": "PSI",
                "STRING": "Testing strings class",
                "COMMOM_ERROR": "Problem during tests in the COMMON classes.",
                "UTILS_ERROR": "Problem during tests in the UTILS classes.",
                "NO_ERROR": "No problem with NPS. " +
                        "End of tests acchieved successfully...",
                "INIT_NPS": "\r " + Bcolors.OKBLUE + " NPS Nernst-Planck Reactive " +
                        "Transport Solver" + Bcolors.ENDC,
                "INIT_OK": Bcolors.ERASE_LINE + Bcolors.OKBLUE + "\rNPS" +
                        Bcolors.ENDC + " initializing reactive transport simulation...",
                "INIT": Bcolors.ERASE_LINE + Bcolors.OKBLUE + "\rNPS" + Bcolors.ENDC +
                        " initializing ",
                "FINISH_0": "\n" + Bcolors.OKBLUE + "NPS" + Bcolors.ENDC,
                "FINISH_1": " Simulation finished successfully (elapsed time: ",
                "FINISH_2": "). Output folder: ",
                "MIN": "min",
                "HRS": "hrs",
                "START_DATE": "Start:",
                "VOLT_PROB": "If voltage is specified, " +
                        "transport equation must be Poisson-Nernst" +
                        "-Planck. Quitting...",
                "EQ_TYPE_PNP": "PNP",
                "END_DATE": "end: ",
                "R_N": "\r\n",
                "HALT": "Halt...\r\n",
        }
