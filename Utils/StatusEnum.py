#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from enum import Enum


# Class Status necessary for evaluation of the progress of the simulation
class StatusEnum(Enum):
    """
    Status enumeration flag that indicates the status of the simulation.
    """
    # Variable to define error status   
    _error = 0
    # Variable to define success status
    _success = 1

    @property
    def get_error(self):
        """
        Returns the error status.
        """
        return self._error

    @property
    def get_success(self):
        """
        Returns the succes status.
        """
        return self._success