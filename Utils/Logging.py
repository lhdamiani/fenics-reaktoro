#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# NPS is open-source code for reactive transport simulations.
#
# Copyright (C) 2018 Leonardo Hax Damiani
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
from Utils.Bcolors import Bcolors
from Utils.StringsNPS import get_string


# Logging class


class Logging:
    """
    Class to provide the IO information methods.
    """

    def __init__(self, *args):
        if len(args) > 0:
            # parameters list
            self._scriptName = args[0]

    def status_print(self, status_message, extra_line):
        # sys.stdout.write(bcolors.ERASE_LINE)
        """
        Method to print the status of the NPS simulation.
        :param status_message: message that will be outputted.
        :param extra_line: Flag indicating extra line necessary.
        """
        sys.stdout.write(status_message)
        sys.stdout.flush()
        if extra_line:
            sys.stdout.write(get_string("R_N"))
            sys.stdout.flush()

    def update_progress(self, progress):
        """
        Updates the progress bar with the current simulation time.
        :param progress: ratio between current simulation time and total simulation time
        """
        bar_length = 50
        status = ""
        if isinstance(progress, int):
            progress = float(progress)
        if not isinstance(progress, float):
            progress = 0
            status = get_string("UPDATE_FLOAT_ERROR")
        if progress < 0:
            progress = 0
            status = get_string("HALT")
        if progress >= 1:
            progress = 1
            status = get_string("R_N")
        block = int(round(bar_length * progress))
        progress_bar_str = Bcolors.CGREYBG + "#" * block + Bcolors.BLACK + \
                           "-" * (bar_length - block) + Bcolors.ENDC
        if progress == 1:
            progress_value = Bcolors.BOLD + str(progress * 100) + Bcolors.ENDC
        else:
            progress_value = str("%0.2f" % (progress * 100,))

        text = Bcolors.OKBLUE + "\rNPS" + Bcolors.ENDC + \
               " : [{0}] {1}% {2}".format(progress_bar_str,
                                          progress_value, status)
        sys.stdout.write(text)
        sys.stdout.flush()

    # Function that prints the final details of the simulations
    def print_finished(self, output_time, output_file_name):
        """
        Method to print the final details of the simulation.
        :param output_time: Time when simulation is finished.
        :param output_file_name: Name of the output directory.
        """
        string_finished0 = get_string("FINISH_0")
        string_finished1 = get_string("FINISH_1")
        string_finished2 = get_string("FINISH_2")
        print string_finished0 + string_finished1 + \
              output_time + string_finished2 + output_file_name
