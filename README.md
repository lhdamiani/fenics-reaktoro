# FEniCS-Reaktoro - Reactive Transport Framework
-------------------------

FEniCS-Reaktoro is a flexible and easily usable reactive transport code coupling the finite element framework FEniCS and the chemical equilibrium solver Reaktoro.

## Getting Started
-------------------------

These instructions will get you a copy of FEniCS-Reaktoro up and running on your local machine for development, modeling and testing purposes.

Choose between Docker containers with a prebuilt image or the source code.

## ![alt text][docker] Docker
[docker]: https://bitbucket.org/lhdamiani/fenics-reaktoro/downloads/rsz_1docker.png 
For end users, we recommend the our FEniCS-Reaktoro prebuilt image, first install [Docker](https://docs.docker.com/install/) for your platform. After that, clone into NPS bitbucket repository:


```
git clone https://bitbucket.org/lhdamiani/fenics-reaktoro.git
```


After this, go to the main folder called `fenics-reaktoro` and execute the installation script:


```
./install/nps_script.sh
```

To run the FEniCS-Reaktoro docker image under a UNIX system, use the command `nps run`. For more options and features, see `nps help`. 

### ![alt text][windows] FEnicS-Reaktoro on windows
[windows]: http://bitbucket.org/lhdamiani/fenics-reaktoro/downloads/rsz_windows_10_logo.jpg
After installing Docker into your system and starting it, download the FEniCS-Reaktoro stable image by running the following command on the windows powershell:

```
docker pull lhdamiani/nps:stable
```

After that, to execute a container with the FEniCS-Reaktoro image run

```
docker run --rm -v <LOCAL_SOURCE_CODE_PATH>:/home/fenics/shared -ti lhdamiani/nps:stable bash
```

changing <LOCAL_SOURCE_CODE_PATH> by your local path to FEniCS-Reaktoro source code (example: c:/Users/example/FEniCS-Reaktoro)


### ![alt text][gears] Souce code
[gears]: https://bitbucket.org/lhdamiani/fenics-reaktoro/downloads/rsz_gears.png
NPS's code is kept in a Bitbucket repository. If you have git installed in your system, download the repository by running the following command:

```
git clone https://bitbucket.org/lhdamiani/nps.git NPS
```

#### Automated testing & coding style guidelines

FEniCS-Reaktoro uses automated Bitbucket Pipeline with a docker image. ![build-status](https://bitbucket-badges.useast.atlassian.io/badge/lhdamiani/NPS.svg)

#### Installing the dependencies  
-------------------------

FEniCS-Reaktoro has dependencies that needs to be installed in your system before running. FEniCS and Reaktoro are the only dependencies necessary and we refer to their installation instructions pages:

- FEniCS: https://fenicsproject.org/download/
- Reaktoro: http://reaktoro.org/installation.html

##### Important:
FEniCS-Reaktoro is developed in [Python](https://www.python.org/) and uses both FEniCS and Reaktoro Python interfaces. Make sure to have the Python interfaces of these dependencies correctly installed. In order to have a fully functional FEniCS-Reaktoro, the dependencies need to be installed in your native system - running FEniCS from the docker image will not work because FEniCS-Reaktoro depends also on Reaktoro. 

### Demos
-------------------------

Along with the docker image or the source code, you will find the directory `Demos`. To run the NaCl diffusion demo, execute the following command from the nps home folder:

```
python nps.py -i ./Demos/NaCl_diffusion/nacl_diffusion.nps
```

### Changelog
-------------------------

See file `CHANGES.rst`.


### License
-------------------------

FEniCS-Reaktoro is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

FEniCS-Reaktoro is distributed WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU general public license, if not please see http://www.gnu.org/licenses/

## Contact
-------------------------

For comments and requests, contact:
```
leonardo.hax@psi.ch
```

[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
